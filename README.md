# Web-shop management system (db-assign#3)
### By Phillip Lunyov ©️
### - Approved ✅

## Tasks

### Idea
The main idea of the project is to develop a small managing system for a web-based shop. The management system will allow a manager to modify and manage accounts (i.e. name, surname, email), manage accounts’ billing & shipment information, manage inventory and vending items, manage products’ definitive information (i.e. discounts, sales, hot sales) and orders. The user will be able to register, authenticate and modify existing self-descriptive information (i.e. billing and shipment data).


### Technical implementation
The following application will be written in a mixture of web-based technologies. The main language for the development would be Python 3 with
Django framework integration (authentication, admin-modules, built-in models, DBMS-handling) for the back-end side. For the front-end, the classical web stack - HTML5, CSS3, VanillaJS with some bootstrap for simple and flexible design. 

The code for the application will be held here, in the repo - for educational purposes.

### Logical model
The E/R diagram can be accessed via [link](er_diagram.png).

### Database scheme
The database scheme can be accessed via [link](db_relation_scheme.png).

### Instructions
The application is tested on Python 3.9 version with all requirements listed in the [file](rq.txt). The following steps should executed to make the application work 
in your local environment:
1. Create a separate directory for the application
2. Create a virtual Python environment in it, setting the base version to 3.9
3. Download the code with git-utility, or by downloading the archive via browser, extracting it into the application directory
4. Active virtual Python environment and download all the required dependencies (dependencies are stored in [file](rq.txt))
5. (optional) Create super user for the application by executing command and following the command prompt: _python manage.py createsuper_
6. Start internal HTTP-server by executing command: _python manage.py runserver_
7. Take a look inside the console, copy the URL of the application and enter it to browser's address bar
8. The application should be accessible

The web management can be accessed via http://_binded IP address_:_binded port number_/admin. The newly created super user and its credentials are crucial to be able to
connect to the administration page. All other functionality will be mentioned in the video.