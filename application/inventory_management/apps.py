from django.apps import AppConfig

class meta_inv_config(AppConfig):
    name = 'inventory_management'
    verbose_name = 'Inventory information'