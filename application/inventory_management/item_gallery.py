# For handling multiple photos for same item-object
from django.db import models

# Custom item object
from .inventory import inventory


class item_gallery(models.Model):

    # Fields
    item_uid = models.ForeignKey(
        to=inventory,
        on_delete=models.CASCADE,
        to_field='item_uid',
        db_column='item_uid',
        related_name='gallery',
        db_index=True,
        parent_link=True,
    )

    supply_image = models.ImageField(
        upload_to='media/supply_photos/',
        verbose_name='Additional image',
        blank=True,
    )

    # Object representation
    def __str__(self):
        return str(self.item_uid)

    class Meta:
        db_table = 'image_gallery'
        verbose_name = 'image'
        verbose_name_plural = 'images'
