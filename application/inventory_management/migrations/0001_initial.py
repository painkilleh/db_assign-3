# Generated by Django 3.1.7 on 2021-03-15 18:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='inventory',
            fields=[
                ('item_uid', models.AutoField(auto_created=True, db_index=True, editable=False, primary_key=True, serialize=False, unique=True, verbose_name='Item UID')),
                ('item_created_at', models.DateTimeField(auto_now_add=True, verbose_name='Item creation timestamp')),
                ('item_name', models.CharField(max_length=100, unique=True, verbose_name="Item's name")),
                ('description', models.TextField(blank=True, max_length=600, verbose_name="Item's description")),
                ('display_image', models.ImageField(blank=True, upload_to='media/main_photos/', verbose_name='Display/Preview image')),
                ('measure', models.CharField(blank=True, default=None, max_length=10, verbose_name='Measure')),
                ('quantity', models.FloatField(default=0.0, verbose_name='Quantity')),
            ],
            options={
                'verbose_name': 'item',
                'verbose_name_plural': 'Inventory',
                'db_table': 'inventory',
                'ordering': ['item_created_at'],
            },
        ),
        migrations.CreateModel(
            name='item_category',
            fields=[
                ('cat_id', models.AutoField(auto_created=True, db_index=True, primary_key=True, serialize=False, unique=True, verbose_name='Category ID')),
                ('name', models.CharField(default='Uncategorized', max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'category',
                'verbose_name_plural': 'categories',
                'db_table': 'item_category',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='item_gallery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item_uid', models.ForeignKey(db_column='item_uid', on_delete=django.db.models.deletion.CASCADE, parent_link=True, related_name='gallery', to='inventory_management.inventory')),
                ('supply_image', models.ImageField(blank=True, upload_to='media/supply_photos/', verbose_name='Additional image')),
            ],
            options={
                'verbose_name': 'image',
                'verbose_name_plural': 'images',
                'db_table': 'image_gallery',
            },
        ),
        migrations.AddField(
            model_name='inventory',
            name='item_category',
            field=models.ForeignKey(db_column='cat_id', on_delete=django.db.models.deletion.DO_NOTHING, to='inventory_management.item_category', verbose_name='Category'),
        ),
    ]
