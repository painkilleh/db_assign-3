# Inventory/warehouse inventory model (complex)
from django.db import models
from django.utils import timezone

# Item category incl.
from .item_category import item_category as IC
# from .item_gallery import item_gallery


class inventory(models.Model):

    # Fields
    item_uid = models.AutoField(
        primary_key=True,
        db_index=True,
        unique=True,
        auto_created=True,
        null=False,
        blank=False,
        editable=False,
        verbose_name='Item UID',
    )

    item_created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False, verbose_name='Item creation timestamp')
    item_name = models.CharField(max_length=100, unique=True, blank=False, null=False, verbose_name='Item\'s name')
    item_category = models.ForeignKey(to=IC, to_field='cat_id', on_delete=models.DO_NOTHING, verbose_name='Category', db_column='cat_id')
    description = models.TextField(max_length=600, blank=True, verbose_name='Item\'s description')
    display_image = models.ImageField(upload_to='media/main_photos/', verbose_name='Display/Preview image', blank=True)
    measure = models.CharField(max_length=10, verbose_name='Measure', default=None, blank=True)
    quantity = models.FloatField(default=0.0, verbose_name='Quantity')

    # Obj represent
    def __str__(self):
        return "{uid} | Name: {name}".format(uid=self.item_uid, name=self.item_name)


    class Meta:
        db_table = 'inventory'
        verbose_name = 'item'
        verbose_name_plural = 'Inventory'
        ordering = ['item_created_at']