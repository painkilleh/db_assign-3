# Inventory model panel registration
from django.contrib import admin

from .inventory import inventory
from .item_category import item_category
from .item_gallery import item_gallery

# Multiple image upload
class inventoryImageGallery(admin.StackedInline):
    model = item_gallery
    extra = 3
    
    
class inventory_admin_meta(admin.ModelAdmin):
    list_display = ('item_uid', 'item_name', 'item_category', 'description', 'quantity', 'item_created_at')
    list_display_links = ('item_uid', 'item_name')
    search_fields = ('item_uid', 'item_name', 'item_category')
    inlines = [inventoryImageGallery]

    list_filter = (
        ('item_created_at', admin.DateFieldListFilter),
        ('quantity', admin.AllValuesFieldListFilter),
    )

class inventory_cat_admin_meta(admin.ModelAdmin):
    list_display = ('cat_id', 'name',)
    list_display_links = ('name',)
    search_fields = ('cat_id', 'name',)

    list_filter = (
        ('name', admin.AllValuesFieldListFilter),
    )


# Actual invoke
admin.site.register(inventory, inventory_admin_meta)
admin.site.register(item_category, inventory_cat_admin_meta)
