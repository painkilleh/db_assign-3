# Item category model
from django.db import models


class item_category(models.Model):

    # Fields
    cat_id = models.AutoField(
        primary_key=True,
        db_index=True,
        auto_created=True,
        unique=True,
        null=False,
        blank=False,
        verbose_name='Category ID',
    )

    name = models.CharField(
        max_length=100,
        blank=False,
        null=False,
        unique=True,
        default='Uncategorized',
    )

    # Obj represent
    def __str__(self):
        return str(self.name)

    class Meta:
        db_table = 'item_category'
        verbose_name = 'category'
        verbose_name_plural = 'categories'
        ordering = ['name']