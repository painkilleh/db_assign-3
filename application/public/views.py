from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate, update_session_auth_hash
from django.contrib import messages
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth.hashers import make_password
from django.contrib.auth.password_validation import validate_password

# Custom Login form
from account.account_forms import AccountLoginForm, AccountRegistrationForm, GeneralAccountForm, AccountBillingForm, AccountShipmentForm, AccountAuthChangeForm
from account.models import Account
from meta_account.billing import billing
from meta_account.shipment import shipment


from django.http import HttpResponse

# Simple log-out functionality with redirect
def log_out(request):

    if request.user.is_authenticated:

        logout(request)
        # Returning to login page
        messages.info(request=request, message='Your session has been terminated.')
    
    return redirect('shop_default')


# Login functionality with redirect
def log_in(request):

    if request.user.is_authenticated:
        return redirect('cp_default')

    if request.method == "POST":

        # Extracting data from request
        form = AccountLoginForm(request.POST)

        if form.is_valid():     

            entered_email = form.cleaned_data.get('email')
            entered_password = form.cleaned_data.get('password')

            pre_user = authenticate(request=request, email=entered_email, password=entered_password)
            
            # If the User-object is fetched
            if pre_user is not None:
                login(request=request, user=pre_user)
                return redirect('cp_default')

    # If method is GET -> return blank form
    else:
        form = AccountLoginForm()

    context = {'form': form }

    return render(request=request, template_name='client_cp/cp_login.html', context=context)


# Registration form
def register_account(request):

    if request.method == "POST":

        register_form = AccountRegistrationForm(request.POST)

        if register_form.is_valid():
            
            # Parsing request and fetching the data for account registration
            email = register_form.cleaned_data.get('email')
            name = register_form.cleaned_data.get('name')
            surname = register_form.cleaned_data.get('surname')
            contact_phone = register_form.cleaned_data.get('contact_phone')

            password = register_form.cleaned_data.get('password')
            password_confirm = register_form.cleaned_data.get('password_confirm')

            new_account = Account.objects.create(email=email, name=name, surname=surname, contact_phone=contact_phone)
            new_account.set_password(password_confirm)
            new_account.save()

            messages.info(request=request, message='Account created successfully. You may authenticate')
            return redirect('login')

    else:
        register_form = AccountRegistrationForm()

    context = { 'form': register_form }
    
    return render(request=request, template_name='client_cp/cp_register.html', context=context)
            

# Account view-collection -> billing/shipment/self-modification
def acc_control_default(request):

    context = {}

    if request.user.is_authenticated is True:
        return render(request=request, template_name='client_cp/cp_profile.html', context=context)

    else:
        messages.error(request=request, message='Please, authenticate yourself before proceeding...')
        return redirect('login')

# Account alteration selection menu
def acc_control_profile_menu(request):

    context = {}

    if request.user.is_authenticated is True:
        return render(request=request, template_name='client_cp/cp_personal.html', context=context)

    else:
        messages.error(request=request, message='Please, authenticate yourself before proceeding...')
        return redirect('login')

# Account personal info change
def acc_control_personal(request):

    if request.user.is_authenticated is True:

        # Trying to fetch existing account and its info        
        try:

            acquired_account = Account.objects.get(uid=request.user.uid)
            account_form = GeneralAccountForm(instance=acquired_account)


        except ObjectDoesNotExist:
            # raise ValidationError(message='Could not fetch account, not existing UID!', code='error')
            messages.error(request=request, message='Fetching not existing account!')
            return redirect('login')

        # POST/GET behaviour
        if request.method == "POST":

            account_form = GeneralAccountForm(request.POST)

            if account_form.is_valid():
                
                # Fetch fields from POST
                acquired_name = account_form.cleaned_data.get('name')
                acquired_surname = account_form.cleaned_data.get('surname')
                acquired_phone = account_form.cleaned_data.get('contact_phone')

                # Saving acquired data
                acquired_account.name = acquired_name.capitalize()
                acquired_account.surname = acquired_surname.capitalize()
                acquired_account.contact_phone = acquired_phone

                acquired_account.save()
                messages.success(request=request, message='The profile has been updated!')

                # Refreshing form
                acquired_account = Account.objects.get(uid=request.user.uid)
                account_form = GeneralAccountForm(instance=acquired_account)

            else:
                messages.error(request=request, message='The form is invalid!')

        context = { 'form': account_form }
        return render(request=request, template_name='client_cp/cp_edit_personal.html', context=context)

    else:
        messages.error(request=request, message='Please, authenticate yourself before proceeding...')
        return redirect('login')


# Account password change 
def acc_control_password(request):

    if request.user.is_authenticated is True:

        auth_change_form = AccountAuthChangeForm()

        if request.method == 'POST':

            auth_change_form = AccountAuthChangeForm(request.POST)

            if auth_change_form.is_valid():

                acquired_current_password = auth_change_form.cleaned_data.get('current_password')
                acquired_new_password = auth_change_form.cleaned_data.get('new_password')
                acquired_new_confirm_password = auth_change_form.cleaned_data.get('confirm_new_password')

                # If new passwords do not match
                if acquired_new_password != acquired_new_confirm_password or not acquired_new_password or not acquired_new_confirm_password:
                    messages.error(request=request, message='Passwords do not match!')
                    return redirect('cp_edit_auth')

                # Fetching user
                modify_user = Account.objects.get(email=request.user.email)
                
                # Trying to acquire user once again to ensure that the current password is known
                if modify_user.check_password(acquired_current_password) is False:
                    messages.error(request=request, message='Wrong current password!')
                    return redirect('cp_edit_auth')

                else:
                    modify_user.set_password(acquired_new_password)
                    modify_user.save()
                    update_session_auth_hash(request=request, user=modify_user)
                    messages.info(request=request, message='Password is updated!')
    
            else:
                messages.error(request=request, message='The form is invalid!')
        else:
            auth_change_form = AccountAuthChangeForm()
        
        context = { 'form': auth_change_form }
        return render(request=request, template_name='client_cp/cp_edit_auth.html', context=context)

    else:
        messages.error(request=request, message='Please, authenticate yourself before proceeding...')
        return redirect('login')


# Account billing information change
def acc_control_billing(request):

    # Flag of control
    freshly_created = False
    
    if request.user.is_authenticated is True:

        # Trying to fetch billing data that is tied to Account
        try:
            acquired_billing_data = billing.objects.get(b_uid=request.user)
        except ObjectDoesNotExist:
            freshly_created = True
            billing_form = AccountBillingForm()


        # POST/GET behaviour
        if request.method == "POST":

            billing_form = AccountBillingForm(request.POST)

            if billing_form.is_valid():

                # Fetch fields from POST
                acquired_name = billing_form.cleaned_data.get('name')
                acquired_surname = billing_form.cleaned_data.get('surname')
                acquired_address = billing_form.cleaned_data.get('address')
                acquired_country = billing_form.cleaned_data.get('country')
                acquired_city = billing_form.cleaned_data.get('city')
                acquired_postcode = billing_form.cleaned_data.get('postcode')

                # If the data existed -> modify and update
                if freshly_created is False:

                    # Saving acquired data
                    acquired_billing_data.name = acquired_name.capitalize()
                    acquired_billing_data.surname = acquired_surname.capitalize()
                    acquired_billing_data.address = acquired_address
                    acquired_billing_data.country = acquired_country.capitalize()
                    acquired_billing_data.city = acquired_city.capitalize()
                    acquired_billing_data.postcode = acquired_postcode

                    acquired_billing_data.save()
                    messages.success(request=request, message='The billing data has been updated!')

                # If not - create instance and fill it properties
                else:
                    
                    freshly_created_billing_data = billing.objects.create(
                        b_uid=request.user,
                        name=acquired_name.capitalize(),
                        surname=acquired_surname.capitalize(),
                        address=acquired_address,
                        country=acquired_country.capitalize(),
                        city=acquired_city.capitalize(),
                        postcode=acquired_postcode
                    )

                    freshly_created_billing_data.save()
                    messages.success(request=request, message='The billing data has been created and updated!')

                # Refreshing form
                acquired_billing_data = billing.objects.get(b_uid=request.user)
                billing_form = AccountBillingForm(instance=acquired_billing_data)

            else:
                messages.error(request=request, message='The form is invalid!')

        # If method -> GET
        else:
            
            # If not init -> fresh form (flag control)
            if freshly_created:
                billing_form = AccountBillingForm()

            # If db has a record -> fetch and display
            else:
                acquired_billing_data = billing.objects.get(b_uid=request.user)
                billing_form = AccountBillingForm(instance=acquired_billing_data)

        context = { 'form': billing_form }
        return render(request=request, template_name='client_cp/cp_edit_billing.html', context=context)

    else:
        messages.error(request=request, message='Please, authenticate yourself before proceeding...')
        return redirect('login')


# Account shipment information change
def acc_control_shipment(request):

    # Flag of control
    freshly_created = False
    
    if request.user.is_authenticated is True:

        # Trying to fetch shipment data that is tied to Account
        try:
            acquired_shipment_data = shipment.objects.get(b_uid=request.user)
        except ObjectDoesNotExist:
            freshly_created = True
            shipment_form = AccountShipmentForm()


        # POST/GET behaviour
        if request.method == "POST":

            shipment_form = AccountShipmentForm(request.POST)

            if shipment_form.is_valid():

                # Fetch fields from POST
                acquired_name = shipment_form.cleaned_data.get('name')
                acquired_surname = shipment_form.cleaned_data.get('surname')
                acquired_address = shipment_form.cleaned_data.get('address')
                acquired_country = shipment_form.cleaned_data.get('country')
                acquired_city = shipment_form.cleaned_data.get('city')
                acquired_postcode = shipment_form.cleaned_data.get('postcode')
                acquired_devivery_phone = shipment_form.cleaned_data.get('delivery_phone')
                acquired_note = shipment_form.cleaned_data.get('note')


                # If the data existed -> modify and update
                if freshly_created is False:

                    # Saving acquired data
                    acquired_shipment_data.name = acquired_name.capitalize()
                    acquired_shipment_data.surname = acquired_surname.capitalize()
                    acquired_shipment_data.address = acquired_address
                    acquired_shipment_data.country = acquired_country.capitalize()
                    acquired_shipment_data.city = acquired_city.capitalize()
                    acquired_shipment_data.postcode = acquired_postcode
                    acquired_shipment_data.delivery_phone = acquired_devivery_phone
                    acquired_shipment_data.note = acquired_note

                    acquired_shipment_data.save()
                    messages.success(request=request, message='The shipment data has been updated!')

                # If not - create instance and fill it properties
                else:
                    
                    freshly_created_shipment_data = shipment.objects.create(
                        b_uid=request.user,
                        name=acquired_name.capitalize(),
                        surname=acquired_surname.capitalize(),
                        address=acquired_address,
                        country=acquired_country.capitalize(),
                        city=acquired_city.capitalize(),
                        postcode=acquired_postcode,
                        delivery_phone=acquired_devivery_phone,
                        note=acquired_note
                    )

                    freshly_created_shipment_data.save()
                    messages.success(request=request, message='The shipment data has been created and updated!')

                # Refreshing form
                acquired_shipment_data = shipment.objects.get(b_uid=request.user)
                shipment_form = AccountShipmentForm(instance=acquired_shipment_data)

            else:
                messages.error(request=request, message='The form is invalid!')
                # print(form.errors)

        # If method -> GET
        else:
            
            # If not init -> fresh form (flag control)
            if freshly_created:
                shipment_form = AccountShipmentForm()

            # If db has a record -> fetch and display
            else:
                acquired_shipment_data = shipment.objects.get(b_uid=request.user)
                shipment_form = AccountShipmentForm(instance=acquired_shipment_data)

        # print(shipment_form.errors)
        context = { 'form': shipment_form }
        return render(request=request, template_name='client_cp/cp_edit_shipment.html', context=context)

    else:
        messages.error(request=request, message='Please, authenticate yourself before proceeding...')
        return redirect('login')
