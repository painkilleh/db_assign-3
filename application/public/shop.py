# View for shop-page (default)
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate, update_session_auth_hash
from django.contrib import messages
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth.hashers import make_password

from django.http import HttpResponse

from vending.vending import vending
from inventory_management.inventory import inventory

# Default shop page
def view_shop_default(request):

    # offer, item_uid

    new_vendings = vending.objects.all()
    # sale_vendings = 
    # hot_vendings = 

    context = { 'new_vendings': new_vendings }
    return render(request=request, template_name='public/shop_default.html', context=context)


# Default vending item page
def view_vending_item(request, uid=None):
    return None