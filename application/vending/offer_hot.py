# Model offer:type - Sale
from django.db import models


class offer_hot(models.Model):

    offer_uid = models.AutoField(
        primary_key=True,
        db_index=True,
        unique=True,
        auto_created=True,
        blank=False,
        null=False,
        verbose_name='Offer UID',
    )

    flag = models.BooleanField(default=True, blank=False, null=False, verbose_name='Hot')
    offer_timestamp = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False, verbose_name='Offer creation timestamp')

    # Internal property
    @property
    def is_hot(self):
        return self.flag

    # Obj represent
    def __str__(self):
        return str('Offer : Hot[UID={uid}]'.format(uid=self.offer_uid))

    class Meta:
        db_table = 'hot_sales'
        verbose_name_plural = 'Hot offers'
        verbose_name = 'hot offer'
        ordering = ['offer_timestamp',]