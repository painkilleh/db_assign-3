# Model for vending
# In essence, MNG-system already has the inventory management, 
# This model is going to use inventory-model as look-up model to display the actual 
# products/items that are going to be sold.j
from django.db import models

# Custom objects
from inventory_management.inventory import inventory


class vending(models.Model):

    # Fields
    lot_uid = models.AutoField(
        primary_key=True,
        db_index=True,
        unique=True,
        auto_created=True,
        blank=False,
        null=False,
        verbose_name='Vending UID',
    )

    item_uid = models.ForeignKey(to=inventory, on_delete=models.DO_NOTHING, verbose_name = 'Item UID', db_column='item_uid')
    vending_created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False, verbose_name='Vending creation timestamp')
    is_active = models.BooleanField(verbose_name='Active flag', default=True)
    price_per_item = models.FloatField(verbose_name='Price per item', blank=False, null=False, default=None)
    selected_quantity = models.FloatField(verbose_name='Selected quanity', default=0.0, blank=False, null=False,
        # validators=[],
    )

    # Obj represen
    def __str__(self):
        return str("Lot UID: {lot}".format(lot=self.lot_uid))

    class Meta:
        db_table = 'vending'
        verbose_name_plural = 'Lots'
        verbose_name = 'lot'
        ordering = ['lot_uid']

