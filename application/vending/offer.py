# Model for Offer-type aggregation
# Holds uid for offer as link for table
from django.db import models

# Custom objects
from .vending import vending
from .offer_new import offer_new
from .offer_sale import offer_sale
from .offer_hot import offer_hot


class offer(models.Model):

    # Fields
    lot_id = models.OneToOneField(to=vending, primary_key=True, on_delete=models.CASCADE, unique=True, null=False, verbose_name='Lot UID', db_column='lot_id')
    offer_new_uid = models.OneToOneField(to=offer_new, on_delete=models.CASCADE, db_column='new_uid', blank=True, null=True, unique=True, default=None)
    offer_sale_uid = models.OneToOneField(to=offer_sale, on_delete=models.CASCADE, db_column='sale_uid', blank=True, null=True, unique=True, default=None)
    offer_hot_uid = models.OneToOneField(to=offer_hot, on_delete=models.CASCADE, db_column='hot_uid', blank=True, null=True, unique=True, default=None)

    # Obj represent
    def __str__(self):
        return str('Lot UID: {uid}'.format(uid=self.lot_id))

    class Meta:
        db_table = 'offers'
        verbose_name_plural = 'Offer types'
        verbose_name = 'offer type'
        ordering = ['lot_id']
