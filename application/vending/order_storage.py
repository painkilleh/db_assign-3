# Model for storing items that belongs to order/cart
# Data is tied to ID of order, with many-to-many relation 
from django.db import models

# Custom models
from .vending import vending
from .order import order

class order_storage(models.Model):

    # Fields
    order_uid = models.ForeignKey(
        to=order,
        to_field='order_uid',
        on_delete=models.CASCADE,
        db_column='order_uid',
        verbose_name='Order UID',
    )

    ordered_vending_item = models.ManyToManyField(
        to=vending,
        db_column='lot_uid',
    )

    ordered_quantity = models.IntegerField(
         default=1,
         verbose_name='Ordered quantity',
    )

    # Obj representation
    def __str__(self):
        return str('Order UID: {order}'.format(order=self.order_uid))
        # return str('Order/Item/Quanity : {order}/{item}/{qt}'.format(order=self.order_group_uid, item=self.ordered_vending_item, qt=self.ordered_quanitity))


    class Meta:
        db_table = 'order_storage'
        verbose_name = 'order item'
        verbose_name_plural = 'orders items'