# Orders-model for storing vending items for certain user in unique record
from django.db import models

from account.models import Account

class order(models.Model):

    # Status choicese
    status_types = {
        ('initialized', 'initialized'),
        ('submitted', 'submitted'),
        ('processing', 'processing'),
        ('finalized', 'finalized'),
        ('error', 'error'),
    }
    
    # Fields
    order_uid = models.AutoField(
        primary_key=True,
        db_index=True,
        unique=True,
        auto_created=True,
        blank=False,
        null=False,
        verbose_name='Order UID',
    )

    acc_uid = models.ForeignKey(to=Account, to_field='uid', on_delete=models.DO_NOTHING, db_column='acc_uid', null=False, blank=False)
    status = models.CharField(choices=status_types, default=0, null=False, blank=False, max_length=20)
    created_ts = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False, verbose_name='Order creation timestamp')

    # Obj repres
    def __str__(self):
        return str('Order UID: {ouid} | Status: {status}'.format(ouid=self.order_uid, status=self.status))

    class Meta:
        db_table = 'orders'
        verbose_name = 'order'
        verbose_name_plural = 'orders'