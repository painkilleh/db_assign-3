from django.contrib import admin

# Register your models here.
from .vending import vending
from .offer import offer
from .offer_new import offer_new
from .offer_sale import offer_sale
from .offer_hot import offer_hot
from .order import order
from .order_storage import order_storage

# Multiple offer selection
class vending_offer_selection(admin.StackedInline):
    model = offer
    extra = 1

# Meta about vending and sub-tabls
class vending_admin_meta(admin.ModelAdmin):
    list_display = ('lot_uid', 'item_uid', 'vending_created_at', 'price_per_item', 'selected_quantity', 'is_active')
    list_display_links = ('lot_uid', 'item_uid')
    search_fields = ('lot_uid', 'price_per_item')
    inlines = [vending_offer_selection]

    list_filter = (
        ('vending_created_at', admin.DateFieldListFilter),
        ('selected_quantity', admin.AllValuesFieldListFilter),
        ('is_active', admin.BooleanFieldListFilter),
    )

class offer_overview_admin_meta(admin.ModelAdmin):
    list_display = ('lot_id',)
    list_display_links = ('lot_id', )
    search_fields = ('lot_id', )


class offer_new_admin_meta(admin.ModelAdmin):
    list_display = ('offer_period', 'offer_timestamp', 'is_new')
    list_display_links = ('offer_timestamp',)
    search_fields = ('offer_timestamp', )

    list_filter = (
        ('offer_timestamp', admin.DateFieldListFilter),
    )

class offer_sales_admin_meta(admin.ModelAdmin):
    list_display = ('offer_start_date', 'offer_end_date', 'discount', 'on_sale', 'offer_timestamp')
    list_display_links = ('offer_start_date', 'offer_timestamp')
    search_fields = ('offer_start_date', 'offer_end_date')

    list_filter = (
        ('offer_timestamp', admin.DateFieldListFilter),
        ('offer_start_date', admin.DateFieldListFilter),
        ('offer_end_date', admin.DateFieldListFilter),
        ('discount', admin.AllValuesFieldListFilter),
    )

class offer_hot_admin_meta(admin.ModelAdmin):
    list_display = ('flag', 'offer_timestamp')
    list_display_links = ('offer_timestamp', )
    search_fields = ('offer_timestamp',)

    list_filter = (
        ('offer_timestamp', admin.DateFieldListFilter),
    )

# Orders
class order_storage_selection(admin.StackedInline):
    model = order_storage
    extra = 1


class order_admin_meta(admin.ModelAdmin):
    list_display = ('order_uid', 'acc_uid', 'status', 'created_ts')
    list_display_links = ('order_uid', 'acc_uid')
    search_fields = ('order_uid', 'acc_uid')
    inlines = [order_storage_selection]

    list_filter = (
        ('created_ts', admin.DateFieldListFilter),
        ('status', admin.AllValuesFieldListFilter),
    )


# Actual invoke
admin.site.register(vending, vending_admin_meta)
admin.site.register(offer, offer_overview_admin_meta)
admin.site.register(offer_new, offer_new_admin_meta)
admin.site.register(offer_sale, offer_sales_admin_meta)
admin.site.register(offer_hot, offer_hot_admin_meta)
admin.site.register(order, order_admin_meta)