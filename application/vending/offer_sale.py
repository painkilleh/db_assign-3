# Model offer:type - Sale
from django.db import models

# Internal libs for timechecks
from datetime import datetime


class offer_sale(models.Model):

    # Fields
    offer_uid = models.AutoField(
        primary_key=True,
        db_index=True,
        unique=True,
        auto_created=True,
        blank=False,
        null=False,
        verbose_name='Offer UID',
    )

    offer_start_date = models.DateTimeField(
        default=None,
        #blank=True,
        #null=True,
        blank=False,
        null=False,
        verbose_name='Offer time (start)',
    )

    offer_end_date = models.DateTimeField(
        default=None,
        # blank=True,
        # null=True,
        blank=False,
        null=False,
        verbose_name='Offer time (end)',
    )

    discount = models.IntegerField(
        default=0,
        # blank=True,
        # null=True,
        blank=False,
        null=False,
        verbose_name='Discount',
    )

    offer_timestamp = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        null=False,
        blank=False,
        verbose_name='Offer creation timestamp'
    )

    # Internal property for form
    @property
    def on_sale(self):
        return bool(datetime.date(datetime.now()) <= datetime.date(self.offer_end_date))

    # Obj represent
    def __str__(self):
        return str('Offer : Sale[UID={uid}]'.format(uid=self.offer_uid))

    class Meta:
        db_table = 'offers_sales'
        verbose_name_plural = 'Offers on sales'
        verbose_name = 'sale'
        ordering = ['offer_end_date', 'offer_timestamp',]