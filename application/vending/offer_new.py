# Model offer:type - New
from django.db import models

# Internal libs for timechecks
from datetime import datetime


class offer_new(models.Model):

    # Fields
    offer_uid = models.AutoField(
        primary_key=True,
        db_index=True,
        unique=True,
        auto_created=True,
        blank=False,
        null=False,
        verbose_name='Offer UID',
    )

    # Day-interval
    offer_period = models.IntegerField(
        default=1,
        null=False,
        blank=False,
        verbose_name='Days to show (interval)'
    )

    offer_timestamp = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        null=False,
        blank=False,
        verbose_name='Offer creation timestamp'
    )

    # Internal property for the form
    @property
    def is_new(self):
        creation_ts = datetime.date(self.offer_timestamp)
        current_ts = datetime.date(datetime.now())
        return bool((current_ts.day - creation_ts.day) <= self.offer_period)

    # Obj represent
    def __str__(self):
        return str('Offer : New[UID={uid}]'.format(uid=self.offer_uid))

    class Meta:
        db_table = 'offers_new'
        verbose_name_plural = 'New offers'
        verbose_name = 'new offer'
        ordering = ['offer_timestamp',]