from django.apps import AppConfig

class meta_data_config(AppConfig):
    name = 'meta_account'
    verbose_name = 'Additional account information'