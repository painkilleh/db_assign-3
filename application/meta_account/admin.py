# Model registration in admin-module with meta
from django.contrib import admin

from meta_account.billing import billing
from meta_account.shipment import shipment


# Billing data registration (with meta)
class billing_meta_admin(admin.ModelAdmin):
    list_display = ('b_uid', 'name', 'surname', 'country', 'city', 'address')
    list_display_links = ('name', 'surname', 'address')
    search_fields = ('name', 'surname', 'address', 'country', 'city', 'postcode')

# Shipment data registration (with meta)
class shipment_meta_admin(admin.ModelAdmin):
    list_display = ('b_uid', 'name', 'surname', 'country', 'city', 'address')
    list_display_links = ('name', 'surname', 'address')
    search_fields = ('name', 'surname', 'address', 'country', 'city', 'postcode')

# Actual invoke
admin.site.register(billing, billing_meta_admin)
admin.site.register(shipment, shipment_meta_admin)