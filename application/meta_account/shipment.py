# Shipping model
from django.db import models
from account.models import Account


class shipment(models.Model):

    # Fields
    b_uid = models.OneToOneField(
        to=Account,
        on_delete=models.CASCADE,
        primary_key=True,
        db_index=True,
        verbose_name='Binding UID',
        unique=True,
        null=False,
        db_column='acc_uid',
        related_name='shipment_data',
    )

    name = models.CharField(max_length=50, null=False, blank=False, verbose_name='Name')
    surname = models.CharField(max_length=50, null=False, blank=False, verbose_name='Surname')
    address = models.CharField(max_length=200, null=False, blank=False, verbose_name='Address')
    country = models.CharField(max_length=100, null=False, blank=False, verbose_name='Country')
    city = models.CharField(max_length=100, null=False, blank=False, verbose_name='City')
    postcode = models.CharField(max_length=30, null=False, blank=False, verbose_name='Postcode')
    delivery_phone = models.CharField(max_length=30, blank=True)
    note = models.TextField(blank=True, max_length=500)
    

    # Object representation
    def __str__(self):
        return str(self.b_uid)


    class Meta:
        db_table = 'shipment_data'
        verbose_name_plural = 'Shipment information'
        verbose_name = 'shipment data entity'
        ordering = ['b_uid']