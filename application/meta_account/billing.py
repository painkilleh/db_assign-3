# Billing model
from django.db import models
from account.models import Account


class billing(models.Model):

    # Fields
    b_uid = models.OneToOneField(
        to=Account,
        on_delete=models.CASCADE,
        db_index=True,
        primary_key=True,
        unique=True,
        null=False,
        verbose_name='Binding UID',
        db_column='acc_uid',
        related_name='billing_data',
    )

    name = models.CharField(max_length=50, null=False, blank=False, verbose_name='Name')
    surname = models.CharField(max_length=50, null=False, blank=False, verbose_name='Surname')
    address = models.CharField(max_length=200, null=False, blank=False, verbose_name='Address')
    country = models.CharField(max_length=100, null=False, blank=False, verbose_name='Country')
    city = models.CharField(max_length=100, null=False, blank=False, verbose_name='City')
    postcode = models.CharField(max_length=30, null=False, blank=False, verbose_name='Postcode')

    # Object representation
    def __str__(self):
        return str(self.b_uid)

    class Meta:
        db_table = 'billing_data'
        verbose_name_plural = 'Billing information'
        verbose_name = 'billing data entity'
        ordering = ['b_uid']