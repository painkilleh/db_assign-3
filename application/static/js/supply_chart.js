// Selecting random value from 0 - 255
function getRandomColourValue() {
    return Math.round(Math.random() * 255);
}

// Returns string rgb(<value>,<value>,<value>)
function getRandomRGB() {
    
    // Random RGB-init
    var r = getRandomColourValue();
    var g = getRandomColourValue();
    var b = getRandomColourValue();

    var answer = 'rgb(' + r + ',' + g + ',' + b + ')'

    return answer
}