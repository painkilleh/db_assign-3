# Custom raw queries to DBMS to fetch information about website
# For the database theory assignment #3
from django.db import connection

# From official Django documentation
# Converts raw SQL query's data into dictionary with 
def dictfetchall(cursor):
    # "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

# Raw query execution
def execute_raw_sql(string_sql, is_view=False):

    with connection.cursor() as cursor:
        cursor.execute(string_sql)

        if is_view is False:
            result = dictfetchall(cursor=cursor)
        else:
            return None

    return result 


# Collection of raw SQL queries to meet the requirements of the task
def first_query():
    return "SELECT DISTINCT inventory.item_uid as 'uid', GROUP_CONCAT(image_gallery.supply_image, ' ,') as 'related' FROM inventory, image_gallery WHERE inventory.item_uid = image_gallery.item_uid GROUP BY inventory.item_uid"


def second_query():
    return "SELECT orders.order_uid as 'Order UID', GROUP_CONCAT(DISTINCT order_storage.id) as 'Order Storage UID' FROM orders, order_storage WHERE orders.order_uid = order_storage.order_uid GROUP BY orders.order_uid"


def third_query():
    return "SELECT * FROM account LEFT JOIN billing_data ON account.uid = billing_data.acc_uid ORDER BY account.uid"


def fourth_query():
    return "SELECT item_category.name, GROUP_CONCAT(inventory.item_name, ' ,') as content, COUNT(*) as counter FROM item_category, inventory WHERE item_category.cat_id = inventory.cat_id GROUP BY item_category.name"


def fifth_query():
    return "CREATE TEMP VIEW user_preview AS SELECT DISTINCT offers.lot_id, offers.new_uid, offers.sale_uid, offers.hot_uid FROM offers WHERE offers.new_uid != 'None' ORDER BY offers.lot_id"

def fifth_query_secondary():
    return "SELECT * FROM user_preview"



