from django.apps import AppConfig


class InfoDashConfig(AppConfig):
    name = 'info_dash'
