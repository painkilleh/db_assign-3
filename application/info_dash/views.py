# DB_assign #3
from django.shortcuts import render
from .models import first_query, second_query, third_query, fourth_query, fifth_query, fifth_query_secondary, execute_raw_sql

# Create your views here.
def display_dashboard(request):

    # Creates view before posting
    result = execute_raw_sql(fifth_query(), is_view=True)

    context = {
        'f_query_info': first_query(),
        'f_query': execute_raw_sql(first_query()),
        's_query_info': second_query(),
        's_query': execute_raw_sql(second_query()),
        'join_query_info': third_query(),
        'join_query': execute_raw_sql(third_query()),
        'aggr_query_info': fourth_query(),
        'aggr_query':execute_raw_sql(fourth_query()),
        'view_query_info': fifth_query(),
        'view_query_secondary': fifth_query_secondary(),
        'view_query': execute_raw_sql(fifth_query_secondary()),
    }

    return render(request=request, template_name='dashboard/dashboard.html', context=context)