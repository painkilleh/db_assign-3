from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, AbstractUser 
from django.utils.translation import gettext, gettext_lazy as _
from django.utils import timezone

class AccountManager(BaseUserManager):

    # Re-defining basic functions for custom handling
    def create_user(self, email, password=None, **extra_fields):

        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            **extra_fields,
        )

        user.set_password(password)
        # user.is_active = True
        user.is_admin = False
        user.save(using=self._db)

        return user

    def create_superuser(self, email, password=None, **extra_fields):

        user = self.create_user(
            email,
            password=password,
            **extra_fields,
        )

        user.is_admin = True
        # user.is_active = True
        user.save(using=self._db)

        return user

# Custom user for all django system
class Account(AbstractBaseUser):

    # Fields
    uid = models.AutoField(
        primary_key=True,
        db_index=True,
        unique=True,
        auto_created=True,
        blank=False,
        null=False,
        verbose_name='Account\'s UID',
    )

    email = models.EmailField(
        max_length=80,
        unique=True,
        verbose_name='email',
        blank=False,
        null=False,
    )

    name = models.CharField(
        max_length=80,
        blank=False,
        null=False,
        verbose_name='Name',
    )

    surname = models.CharField(
        max_length=80,
        blank=False,
        null=False,
        verbose_name='Surname',
    )

    contact_phone = models.CharField(
        max_length=80,
        verbose_name='Contact phone',
        blank=True,
    )

    date_joined = models.DateTimeField(
        auto_created=True,
        default=timezone.now,
        verbose_name='Registration datetime',
        editable=False,
    )

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    # Acquire last_login via parent-class inheritance (timestamp)

    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    class Meta:
        db_table = 'account'
        verbose_name_plural = 'Accounts'
        verbose_name = 'account'
        ordering = ['date_joined']

    def __str__(self):
        return "UID: {uid} | email: {email}".format(uid=self.uid, email=self.email)

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

    # Custom functions
    @property
    def getFullAccountName(self):
        return "{name} {surname}".format(name=self.name, surname=self.surname)