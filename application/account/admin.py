from django.contrib import admin
from django.contrib.auth.models import Group, User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

# Defined user-models
from .models import Account


# Account-Model
class CustomUserAdmin(BaseUserAdmin):

    model = Account
    # add_form = CustomUserCreationForm

    # Custom template 
    add_form_template = 'admin/application/admin_custom_add_form.html'

    # Display/filters for djando admin
    list_display = ('uid', 'email', 'name', 'surname', 'contact_phone', 'is_admin', 'date_joined', 'last_login')
    list_filter = ('is_admin', 'is_active') 
    list_display_links = ('email', )

    list_filter = (
        ('last_login', admin.DateFieldListFilter),
        ('date_joined', admin.DateFieldListFilter),
        ('is_admin', admin.BooleanFieldListFilter),
    )

    # Fieldsets for CRUD functionality
    fieldsets = (
        ('Authentication', {'fields': ('email', 'password')}),
        ('Personal data', {'fields': ('name', 'surname', 'contact_phone')}),
        ('Permissions', {'fields': ('is_admin', 'is_active', )}),
    )

    add_fieldsets = (
        ('Personal data', {
            'classes' : ('wide', ),
            'fields' : ('email', 'name', 'surname', 'contact_phone')
        }),

        ('Authentication', {
            'classes' : ('wide', ), 
            'fields' : ('password1', 'password2' ),
        }),

        ('Permissions', {
            'classes' : ('wide', ), 
            'fields' : ('is_active', 'is_admin', ),
        }),

    )

    # Filtering and search options
    search_fields = ('email', 'uid')
    ordering = ['email', 'date_joined']
    filter_horizontal = ()

    

# Disabling user
admin.site.register(Account, CustomUserAdmin)
admin.site.unregister(Group)