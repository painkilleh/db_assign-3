# Account-related forms
from django import forms
from django.db import models
from django.utils.translation import gettext, gettext_lazy as _
from django.core.exceptions import ValidationError
from django.contrib.auth.password_validation import validate_password

from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm

# Custom models
from .models import Account
from meta_account.billing import billing
from meta_account.shipment import shipment

class AccountLoginForm(forms.Form):

    # Fields for form + bootstrap classes pre-appliance
    email = forms.EmailField(
        max_length=80,
        required=True,
        widget=forms.TextInput(
            attrs={
                'type':'email',
                'name':'email',
                'id':'email',
                'class':'form-control',
                'placeholder':'Email',
            },
        )
    )

    password = forms.CharField(
        label=_('Password'),
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'type':'password',
                'name':'password',
                'id':'password',
                'class':'form-control',
                'placeholder':'Password'
            },
        ),
    )

    def clean(self):
        
        cleaned_data = super(AccountLoginForm, self).clean()
        email = cleaned_data.get('email')
        password = cleaned_data.get('password')

        # Test of exceptions and its handling
        if not email and not password:
            raise ValidationError(_('You have to input email or password to be able to authenticate!'), code='error')

        if email and not password or not email and password:
            raise ValidationError(_('Email or password is missing in the form!'), code='error')


class AccountRegistrationForm(forms.Form):
    
    # Fields account registration
    email = forms.EmailField(
        max_length=80,
        required=True,
        label=_('Email address'),
        widget=forms.TextInput(
            attrs={
                'type':'email',
                'name':'email',
                'id':'email',
                'class':'form-control',
                'placeholder':'Your email address',
            },
        )
    )

    name = forms.CharField(
        label=_('Name'),
        required=True,
        widget=forms.TextInput(
            attrs={
                'type':'name',
                'name':'name',
                'id':'name',
                'class':'form-control',
                'placeholder':'Your name',
            }
        )
    )

    surname = forms.CharField(
        label=_('Surname'),
        required=True,
        widget=forms.TextInput(
            attrs={
                'type':'surname',
                'name':'surname',
                'id':'surname',
                'class':'form-control',
                'placeholder':'Your surname',
            }
        )
    )

    password = forms.CharField(
        label=_('Password'),
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'type':'password',
                'name':'password',
                'id':'password',
                'class':'form-control',
                'placeholder':'Desired password',
            }
        )
    )

    password_confirm = forms.CharField(
        label=_('Password confirmation'),
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'type':'password',
                'name':'password',
                'id':'password',
                'class':'form-control',
                'placeholder':'Re-enter password',
            }
        )

    )

    contact_phone = forms.CharField(
        label=_('Contact phone'),
        widget=forms.TextInput(
            attrs={
                'type':'phone',
                'name':'phone',
                'id':'phone',
                'class':'form-control',
                'placeholder':'Your contact phone',
            }
        )
    )

    
    def clean(self):
        cleaned_data = super(AccountRegistrationForm, self).clean()

        email = cleaned_data.get('email')
        name = cleaned_data.get('name').capitalize()
        surname = cleaned_data.get('surname').capitalize()
        contact_phone = cleaned_data.get('contact_phone')

        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('password_confirm')

        if password != password_confirm or not password and password_confirm or password and not password_confirm:
            raise ValidationError(_('Passwords do not match!'), code='error')


class GeneralAccountForm(forms.ModelForm):

    class Meta:
        model = Account
        fields = ('name', 'surname', 'contact_phone')
        exclude = ('uid', 'password', 'is_active', 'is_admin', 'date_joined')

        widgets = {
            'name': forms.TextInput(attrs={
                'type':'name',
                'name':'name',
                'id':'name',
                'class':'form-control',
                'placeholder':'Your name',
            }),

            'surname': forms.TextInput(attrs={
                'type':'surname',
                'name':'surname',
                'id':'surname',
                'class':'form-control',
                'placeholder':'Your surname',
            }),

            'contact_phone': forms.TextInput(attrs={
                'type':'contact_phone',
                'name':'contact_phone',
                'id':'contact_phone',
                'class':'form-control',
                'placeholder':'Your contact phone',
            })
        }

        labels = {
            'name': _('Name'),
            'surname': _('Surname'),
            'contact_phone': _('Contact phone')
        }


class AccountBillingForm(forms.ModelForm):
    
    class Meta:
        model = billing
        fields = ('name', 'surname', 'address', 'country', 'city', 'postcode')
        exclude = ('b_uid', )

        widgets = {
            'name': forms.TextInput(attrs={
                'type':'name',
                'name':'name',
                'id':'name',
                'class':'form-control',
                'placeholder':'Your name',
            }),

            'surname': forms.TextInput(attrs={
                'type':'surname',
                'name':'surname',
                'id':'surname',
                'class':'form-control',
                'placeholder':'Your surname',
            }),

            'address': forms.TextInput(attrs={
                'type':'address',
                'name':'address',
                'id':'address',
                'class':'form-control',
                'placeholder':'Your address',
            }),

            'country': forms.TextInput(attrs={
                'type':'country',
                'name':'country',
                'id':'country',
                'class':'form-control',
                'placeholder':'Country of origin',
            }),

            'city': forms.TextInput(attrs={
                'type':'city',
                'name':'city',
                'id':'city',
                'class':'form-control',
                'placeholder':'City',
            }),

            'postcode': forms.TextInput(attrs={
                'type':'postcode',
                'name':'postcode',
                'id':'postcode',
                'class':'form-control',
                'placeholder':'Postcode',
            }),
        }

        labels = {
            'name': _('Name'),
            'surname': _('Surname'),
            'address': _('Address'),
            'country':_('Country'),
            'city':_('City'),
            'postcode':_('Postcode'),
        }


class AccountShipmentForm(forms.ModelForm):

        class Meta:
            model = shipment
            fields = ('name', 'surname', 'address', 'country', 'city', 'postcode', 'delivery_phone', 'note')
            exclude = ('b_uid', )

            widgets = {
                'name': forms.TextInput(attrs={
                    'type':'name',
                    'name':'name',
                    'id':'name',
                    'class':'form-control',
                    'placeholder':'Your name',
                }),

                'surname': forms.TextInput(attrs={
                    'type':'surname',
                    'name':'surname',
                    'id':'surname',
                    'class':'form-control',
                    'placeholder':'Your surname',
                }),

                'address': forms.TextInput(attrs={
                    'type':'address',
                    'name':'address',
                    'id':'address',
                    'class':'form-control',
                    'placeholder':'Your address',
                }),

                'country': forms.TextInput(attrs={
                    'type':'country',
                    'name':'country',
                    'id':'country',
                    'class':'form-control',
                    'placeholder':'Country of origin',
                }),

                'city': forms.TextInput(attrs={
                    'type':'city',
                    'name':'city',
                    'id':'city',
                    'class':'form-control',
                    'placeholder':'City',
                }),

                'postcode': forms.TextInput(attrs={
                    'type':'postcode',
                    'name':'postcode',
                    'id':'postcode',
                    'class':'form-control',
                    'placeholder':'Postcode',
                }),

                'delivery_phone': forms.TextInput(attrs={
                    'type':'delivery_phone',
                    'name':'delivery_phone',
                    'id':'delivery_phone',
                    'class':'form-control',
                    'placeholder':'Phone number',
                }),

                'note': forms.TextInput(attrs={
                    'type':'note',
                    'name':'note',
                    'id':'note',
                    'class':'form-control',
                    'placeholder':'Note for delivery (optional)',
                }), 
            }

            labels = {
                'name': _('Name'),
                'surname': _('Surname'),
                'address': _('Address'),
                'country':_('Country'),
                'city':_('City'),
                'postcode':_('Postcode'),
                'delivery_phone':_('Contact phone'),
                'note':_('Note'),
            }


class AccountAuthChangeForm(forms.Form):

    current_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'type':'password',
                'name':'password',
                'id':'password',
                'class':'form-control',
                'placeholder':'Current password',
            }
        )
    )

    new_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'type':'password',
                'name':'new_password',
                'id':'new_password',
                'class':'form-control',
                'placeholder':'Enter new password',
            }
        ),

        validators=[
            validate_password
        ],
    )

    confirm_new_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'type':'password',
                'name':'confirm_new_password',
                'id':'confirm_new_password',
                'class':'form-control',
                'placeholder':'Confirm new password',
            }
        ),

        validators=[
            validate_password
        ],
    )

    def clean(self):        
        # Acquiring data
        cleaned_data = super().clean()

        # Account manager (internal)
        # AM = Account.objects

        # self.acquired_current_pass = self.cleaned_data['current_password']
        # self.acquired_new_pass = self.cleaned_data['new_password']
        # self.acquired_confirm_pass = self.cleaned_data['confirm_new_password']

        