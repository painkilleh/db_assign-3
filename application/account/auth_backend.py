# Custom authentication model 
# Due to the change of authentication from user+pass -> email+pass
from django.contrib.auth.backends import BaseBackend, ModelBackend
from django.core.exceptions import ValidationError
from .models import Account


from django.contrib.auth import authenticate, login, logout

class AccountAuthBackend(ModelBackend):

    def authenticate(self, email, password):

        if email is None:
            raise ValidationError('Email is missing!')

        if password is None:
            raise ValidationError('Password is missing!')

        if email and password:

            try:
                request_account = Account.objects.get(email=email)
            except Exception:
                raise ValidationError('Could not fetch account!')
            
            return request_account

        return None


    def get_user(self, email):
        try:
            return Account.objects.get(email=email)
        except Exception:
            return None