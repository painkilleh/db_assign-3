"""application URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views

from django.conf import settings
from django.conf.urls.static import static

# Defined views
from public.views import log_in, log_out, register_account, acc_control_default, acc_control_profile_menu, acc_control_personal, acc_control_billing, acc_control_shipment, acc_control_password
from public.shop import view_shop_default
from info_dash.views import display_dashboard

urlpatterns = [
    # Shop default
    path('', view_shop_default, name='shop_default'), 
    # Vending item selection
    # path('vending/<int:uid>/, verbose_vending_item, name='show_vending_item'),
    # Maybe, display cart 
    # path('profile/current_order/', view_current_order, name='order_default'),
    path('login/', log_in, name='login'),
    path('logout/', log_out, name='logout'),
    path('register/', register_account, name='register'),
    path('profile/', acc_control_default, name='cp_default'),
    path('profile/edit/', acc_control_profile_menu, name='cp_edit_default'),
    path('profile/edit/personal/', acc_control_personal, name='cp_edit_personal'),
    path('profile/edit/personal/auth/', acc_control_password, name='cp_edit_auth'),
    path('profile/edit/billing/', acc_control_billing, name='cp_edit_billing'),
    path('profile/edit/shipment/', acc_control_shipment, name='cp_edit_shipment'),
    # Dashboard 
    path('dashboard/', display_dashboard, name='dashboard'),
    # Django-admin module
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)